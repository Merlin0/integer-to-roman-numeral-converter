def IntegerToRomanNumeralConverter(n):
    if n > 3999 or n < 0 or not isinstance(n, int):
        return False
    
    roman_dict = {
        1000: "M", 900: "CM", 500: "D", 400: "CD",
        100: "C", 90: "XC", 50: "L", 40: "XL",
        10: "X", 9: "IX", 5: "V", 4: "IV",
        1: "I"
    }

    roman_value = ""
    
    for value, symbol in roman_dict.items():
        while n >= value:
            roman_value += symbol
            n -= value
    return roman_value


print(IntegerToRomanNumeralConverter(10.5))
