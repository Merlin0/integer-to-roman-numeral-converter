from unittest import TestCase
from IntegerToRomanNumeralConverter import IntegerToRomanNumeralConverter


class TestFizzBuzz(TestCase):

    def test_IntegerToRomanNumeralConverter_falseNumbers(self):
        test_cases = [5000, -1, 10.5]
        for test in test_cases:
            result = IntegerToRomanNumeralConverter(test)
            self.assertFalse(result)

    def test_IntegerToRomanNumeralConverter_basicNumbers(self):
        test_cases = {
            1: "I",
            5: "V",
            10: "X",
            50: "L",
            100: "C",
            500: "D",
            1000: "M"
        }

        for test, expected_result in test_cases.items():
            result = IntegerToRomanNumeralConverter(test)
            self.assertEqual(result, expected_result)

    def test_IntegerToRomanNumeralConverter_advancedNumbers(self):
        test_cases = {
            4: "IV",
            6: "VI",
            9: "IX",
            11: "XI",
            25: "XXV",
            90: "XC",
            400: "CD",
            900: "CM",
            1100: "MC"
        }

        for test, expected_result in test_cases.items():
            result = IntegerToRomanNumeralConverter(test)
            self.assertEqual(result, expected_result)

    def test_IntegerToRomanNumeralConverter_varyAdvancedNumbers(self):
        test_cases = {
            1066: "MLXVI",
            1111: "MCXI",
            1776: "MDCCLXXVI",
            1944: "MCMXLIV"
        }

        for test, expected_result in test_cases.items():
            result = IntegerToRomanNumeralConverter(test)
            self.assertEqual(result, expected_result)
